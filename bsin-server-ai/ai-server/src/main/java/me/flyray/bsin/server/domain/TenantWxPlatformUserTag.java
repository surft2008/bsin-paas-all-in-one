package me.flyray.bsin.server.domain;

import java.io.Serializable;

import lombok.Data;

/**
 * 
 * @TableName ai_tenant_wxmp_user_tag
 */
@Data
public class TenantWxPlatformUserTag {

    /**
     * 
     */
    private String serialNo;


    private String tenantId;

    /**
     * 用户openId
     */
    private String openId;

    /**
     * 标签
     */
    private String tag;

    /**
     * 应用ID
     */
    private String appId;

}