package me.flyray.bsin.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * @author ：bolei
 * @date ：Created in 2021/11/30 16:00
 * @description：bsin 脚手架启动类
 * @modified By：
 */

@ImportResource({"classpath*:sofa/rpc-provider-hello.xml"})
@SpringBootApplication
public class BsinWorkflowAdminApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(BsinWorkflowAdminApplication.class);
        springApplication.run(args);
    }

}
