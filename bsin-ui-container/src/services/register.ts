import bsinRequest from '@/utils/bsinRequest';

// 注册接口
export const userRegister = (params: any) => {
  return bsinRequest('/register', {
    serviceName: 'MerchantService',
    methodName: 'register',
    version: '1.0',
    bizParams: {
      ...params,
    },
  });
};
