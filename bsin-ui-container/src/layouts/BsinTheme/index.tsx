import {
  DesktopOutlined,
  FileOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Link, AppsState, connect } from 'umi';
import type { MenuProps } from 'antd';
import { Breadcrumb, Layout, Menu, Space } from 'antd';
import React, { useState } from 'react';
import BsinLayoutContext, {
  AppMenu,
} from '../stores/BsinLayoutContext';

const { Header, Content, Footer, Sider } = Layout;

import CompMenu from './components/menu';

import "./index.css"
import "./index.less"

export type USerInfo = {
  name: string;
  headerUrl: string;
};

export type BsinThemLayoutProps = {
  appMenu?: AppMenu;
  userInfo: USerInfo;
  children: any;
  apps: AppsState;
};

const BsinThemeLayout: React.FC<BsinThemLayoutProps> = ({
  appMenu,
  userInfo,
  children,
  apps
}) => {
  return (
    <BsinLayoutContext.Provider
      initialState={{
        userInfo,
        appMenu,
      }}
    >
      <div style={{ width: '100%', display: 'flex', flexDirection: 'row' }} >
        <CompMenu apps={apps}/>
        <Layout className="site-layout" style={{ overflow: 'auto', height: '100dvh', maxHeight: 'none' }}>
          <Content style={{ margin: '0' }}>
            <div className="site-layout-background" style={{    overflow: 'auto',height: 'calc(100vh - 70px)' }}>
              {children}
            </div>
            <Footer style={{ textAlign: 'center' }}>jiujiu ©2023 Created by s11eDao</Footer>
          </Content>
        </Layout>
      </div>
    </BsinLayoutContext.Provider>
  );
};


const mapStateToProps = ({
  apps,
}: {
  apps: AppsState;
}) => {
  return {
    apps,
  };
};

export default connect(mapStateToProps)(BsinThemeLayout);
