package me.flyray.bsin.facade.service;

import java.util.Map;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * @author leonard
 * @description 针对表【sys_service_subscribe】的数据库操作Service
 * @createDate 2024-01-24 21:11:52
 */
@Path("serviceSubscribe")
public interface ServiceSubscribeService {

  /** 新增 */
  @POST
  @Path("add")
  @Produces("application/json")
  public Map<String, Object> add(Map<String, Object> requestMap);

  /** 删除 */
  @POST
  @Path("delete")
  @Produces("application/json")
  public Map<String, Object> delete(Map<String, Object> requestMap);

  /** 更新 */
  @POST
  @Path("edit")
  @Produces("application/json")
  public Map<String, Object> edit(Map<String, Object> requestMap);

  /** 根据条件查询列表 */
  @POST
  @Path("getList")
  @Produces("application/json")
  public Map<String, Object> getList(Map<String, Object> requestMap);

  /** 根据条件查询列表 */
  @POST
  @Path("getPageList")
  @Produces("application/json")
  public Map<String, Object> getPageList(Map<String, Object> requestMap);

}
