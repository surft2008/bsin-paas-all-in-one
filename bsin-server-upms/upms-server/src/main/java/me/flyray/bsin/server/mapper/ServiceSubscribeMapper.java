package me.flyray.bsin.server.mapper;

import java.util.List;
import me.flyray.bsin.server.domain.SysServiceSubscribe;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author leonard
 * @description 针对表【sys_service_subscribe】的数据库操作Mapper
 * @createDate 2023-11-05 17:59:52 @Entity generator.domain.SysServiceSubscribe
 */
@Repository
@Mapper
public interface ServiceSubscribeMapper {

  List<SysServiceSubscribe> selectList(@Param("customerNo") String customerNo);

  public void insert(SysServiceSubscribe serviceSubscribe);

  public void deleteById(String id);

  public void updateById(SysServiceSubscribe serviceSubscribe);
}
